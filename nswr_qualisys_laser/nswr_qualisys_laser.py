import rclpy
from rclpy.node import Node
from nav_msgs.msg import Odometry
from std_msgs.msg import Header
from sensor_msgs.msg import PointCloud2, PointField
from geometry_msgs.msg import Quaternion, TransformStamped, PoseStamped
from tf2_ros import TransformException, TransformBroadcaster
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
import sensor_msgs_py.point_cloud2 as pc2
from tf_transformations import quaternion_matrix, quaternion_from_euler
import numpy as np
import math


class Qualisys(Node):

    def __init__(self):
        super().__init__('nswr_qualisys_laser')

        # Point cloud subscriber
        self.velodyne_sub = self.create_subscription(PointCloud2,'/velodyne/velodyne_points',self.velodyne_callback,1)

        # Define the subscriber of helmet poses, use self.helmet_pose_callback as callback function
        # self.helmet_pose_sub = ...

        # Advertise topic with the transformed points
        self.points_pub = self.create_publisher(PointCloud2,'/transformed_point_cloud', 1)

        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)
        self.tf_broadcaster = TransformBroadcaster(self)
        self.qualisys2helmet = None
        self.helmet2laser = None
        self.timer = self.create_timer(0.1, self.node_callback)

        
        self.velodyne_fields = [
            PointField(name='x',
                       offset=0,
                       datatype=PointField.FLOAT32,
                       count=1),
            PointField(name='y',
                       offset=4,
                       datatype=PointField.FLOAT32,
                       count=1),
            PointField(name='z',
                       offset=8,
                       datatype=PointField.FLOAT32,
                       count=1),
        ]

    def helmet_pose_callback(self, msg):

        if self.qualisys2helmet is None:
            self.qualisys2helmet = TransformStamped()

        # Fill the self.qualisys2helmet message
        self.qualisys2helmet.header.stamp = msg.header.stamp
        # ...

        # Send the transformation
        # self.tf_broadcaster.sendTransform(self.qualisys2helmet)
        
    def velodyne_callback(self, msg):

        if self.helmet2laser is None or self.qualisys2helmet is None:
            return

        # Converting ROS message to point list
        points = pc2.read_points_list(msg,["x", "y", "z"])
        
        # Convert self.qualisys2helmet to 4x4 matrix
        # ...

        # Convert self.helmet2laser to 4x4 matrix
        # ...

        # Transform all points from local to the qualisys frame 


        
        # Convert created points to ROS message
        header = Header()
        header.frame_id = "qualisys"
        # transformed_point_cloud = pc2.create_cloud()
        
        # Publish transformed point cloud
        # self.points_pub.publish()

    def node_callback(self):

        # Reads the transform between "helmet" and "laser" frame
        if self.helmet2laser is None:
            try:
                self.helmet2laser = self.tf_buffer.lookup_transform("helmet","laser",rclpy.time.Time())
            except TransformException as ex:
                return
    

def main(args=None):

    rclpy.init(args=args)
    qualisys = Qualisys()

    rclpy.spin(qualisys)

    qualisys.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()